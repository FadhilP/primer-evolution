import random

class Entity:

    #Static variables
    entity_count = 0;
    entityList = []

    def __init__(self, birth_rate, death_rate, replication_rate):
        if birth_rate > random.random():
            self.birth_rate = birth_rate
            self.death_rate = death_rate
            self.replication_rate = replication_rate
            Entity.entityList.append(self)
            Entity.entity_count += 1

    def run(self):
        if self.death_rate > random.random():
            Entity.entity_count -= 1
            Entity.entityList.remove(self)

        elif self.replication_rate > random.random():
            Entity(self.birth_rate, self.death_rate, self.replication_rate)
        
        