from Entity import *
from matplotlib import pyplot as plt
import time
import random
import numpy as np

print("Enter birth rate: ", end = '')
BIRTH_RATE = eval(input())

print("Enter death rate: ", end = '')
DEATH_RATE = eval(input())

print("Enter replication rate: ", end = '')
REPLICATION_RATE = eval(input())

print("Enter number of initial entity: ", end = '')
INITIAL_ENTITY_COUNT = eval(input())
for i in range(INITIAL_ENTITY_COUNT):
    Entity(BIRTH_RATE, DEATH_RATE, REPLICATION_RATE)

print("Enter how many days the simulation will run: ", end = '')
day_count = eval(input())
DAYS = [x for x in range(day_count)]

#Data represents total number of entities every day
data = np.array([])

#Delta represents total number of difference on entities every day
delta = np.array([])

for i in DAYS:
    
    Entity(BIRTH_RATE, DEATH_RATE, REPLICATION_RATE)

    for entity in Entity.entityList:
        entity.run()

    try:
        delta = np.append(delta, entity.entity_count - data[-1])
    except:
        delta = np.append(delta, 0)

    data = np.append(data, entity.entity_count)
    

print("\nAverage number of entity: ", np.mean(data))

plt.plot(DAYS, data)
plt.show()


